import tempfile

import pandas as pd

from dbc.classes import Field, Schema

schema = Schema(
    fields=[
        Field(name="int", type="integer"),
        Field(name="float", type="number"),
        Field(name="str", type="string"),
        Field(name="bool", type="boolean"),
        Field(name="date", type="date"),
        Field(name="datetime", type="datetime"),
    ]
)

df = pd.DataFrame(
    {
        "int": pd.Series(["1", pd.NA, "3", "4"]),
        "float": pd.Series(["1.1", "2.2", "3.3", "4.4"]),
        "str": pd.Series(["a", pd.NA, "c", "d"]),
        "bool": pd.Series(["true", pd.NA, "true", "false"]),
        "date": pd.Series(["2021-01-01", pd.NA, "2021-01-03", "2021-01-04"]),
        "datetime": pd.Series(
            ["2021-01-01T01:00:00", pd.NA, "2021-01-03T01:00:00", "2021-01-03T01:00:00"]
        ),
        "extra": pd.NA,
    }
)

chunks = slice(0, 3), slice(3, 4)
files = [""] * len(chunks)
for i, chunk in enumerate(chunks):
    _, files[i] = tempfile.mkstemp(suffix=".csv")
    df[chunk].to_csv(files[i], index=False)


def test_reads_single_csv() -> None:
    result = schema.read_csv(files[0], parse_dtypes=False)
    expected = pd.DataFrame(
        {
            "int": pd.Series(["1", pd.NA, "3"]),
            "float": pd.Series(["1.1", "2.2", "3.3"]),
            "str": pd.Series(["a", pd.NA, "c"]),
            "bool": pd.Series(["true", pd.NA, "true"]),
            "date": pd.Series(["2021-01-01", pd.NA, "2021-01-03"]),
            "datetime": pd.Series(
                ["2021-01-01T01:00:00", pd.NA, "2021-01-03T01:00:00"]
            ),
        }
    )
    pd.testing.assert_frame_equal(result, expected)
    # Include extra columns
    result = schema.read_csv(files[0], parse_dtypes=False, read_all_columns=True)
    expected["extra"] = pd.NA
    pd.testing.assert_frame_equal(result, expected)


def test_reads_multiple_csv() -> None:
    result = schema.read_csv(files, parse_dtypes=False)
    expected = pd.DataFrame(
        {
            "int": pd.Series(["1", pd.NA, "3", "4"]),
            "float": pd.Series(["1.1", "2.2", "3.3", "4.4"]),
            "str": pd.Series(["a", pd.NA, "c", "d"]),
            "bool": pd.Series(["true", pd.NA, "true", "false"]),
            "date": pd.Series(["2021-01-01", pd.NA, "2021-01-03", "2021-01-04"]),
            "datetime": pd.Series(
                [
                    "2021-01-01T01:00:00",
                    pd.NA,
                    "2021-01-03T01:00:00",
                    "2021-01-03T01:00:00",
                ]
            ),
        }
    )
    pd.testing.assert_frame_equal(result, expected)
    # Include extra columns
    result = schema.read_csv(files, parse_dtypes=False, read_all_columns=True)
    expected["extra"] = pd.NA
    pd.testing.assert_frame_equal(result, expected)


def test_parses_dates() -> None:
    result = schema.read_csv(files, parse_dates=True)
    expected = pd.DataFrame(
        {
            "date": pd.Series(
                ["2021-01-01", pd.NA, "2021-01-03", "2021-01-04"],
                dtype="datetime64[ns]",
            ),
            "datetime": pd.Series(
                [
                    "2021-01-01T01:00:00",
                    pd.NA,
                    "2021-01-03T01:00:00",
                    "2021-01-03T01:00:00",
                ],
                dtype="datetime64[ns]",
            ),
        }
    )
    pd.testing.assert_frame_equal(result[["date"]], expected[["date"]])
    pd.testing.assert_frame_equal(result[["datetime"]], expected[["datetime"]])
