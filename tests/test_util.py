from pathlib import Path

import dbc.util


def test_reads_nested_yaml() -> None:
    path = "tests/yml/index.yml"
    parent = {"array": [0, 1], "object": {"key": "value"}, "scalar": 0}
    expected = {"parent": parent, "parent_in_array": [parent]}
    result = dbc.util.read_yaml(path)
    assert result == expected


def test_writes_simple_markdown_template() -> None:
    path = "tests/template/package.yml"
    descriptor = dbc.util.read_yaml(path)
    txt = dbc.util.json_to_markdown(descriptor)
    outpath = Path("temp", "package-simple.md")
    dbc.util.write_file_path(outpath)
    with open(outpath, "w") as f:
        f.write(txt)


def test_writes_fancy_markdown_template() -> None:
    path = "tests/template/package.yml"
    descriptor = dbc.util.read_yaml(path)
    txt = dbc.util.render_template(
        "src/dbc/templates/package.md", {"package": descriptor}
    )
    outpath = Path("temp", "package-fancy.md")
    dbc.util.write_file_path(outpath)
    with open(outpath, "w") as f:
        f.write(txt)


def test_writes_markdown_template_with_table() -> None:
    path = "tests/template/main.yml"
    descriptor = dbc.util.read_yaml(path)
    txt = dbc.util.render_template(
        "src/dbc/templates/resource-table.md", {"resource": descriptor}
    )
    outpath = Path("temp", "resource-table.md")
    with open(outpath, "w") as f:
        f.write(txt)
