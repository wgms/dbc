from pathlib import Path

from frictionless import Package

from dbc.spreadsheets.excel import Template
import dbc.util


def test_writes_excel_template() -> None:
    path = "tests/template/package.yml"
    descriptor = dbc.util.read_yaml(path)
    package = Package(descriptor)
    outpath = Path("temp", "package.xlsx")
    dbc.util.write_file_path(outpath)
    template = Template(package, path=outpath)
    template.build(
        freeze_header=True,
        format_header={"bold": True, "bg_color": "#d3d3d3"},
        format_condition={"bg_color": "#ffc7ce"},
    )
