import click.testing
import pytest

from dbc import console


@pytest.fixture
def runner() -> click.testing.CliRunner:
    return click.testing.CliRunner()


def test_main_succeeds(runner: click.testing.CliRunner) -> None:
    result = runner.invoke(console.main)
    assert result.exit_code == 0
