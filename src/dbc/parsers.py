from typing import Any, Callable, List

import pandas as pd


def _coerce(ds: pd.Series, dtype: Callable, na: Any = pd.NA) -> pd.Series:
    def func(x: Any) -> Any:
        if pd.isna(x):
            return na
        try:
            return dtype(x)
        except (ValueError, TypeError):
            return na

    return ds.map(func)


def parse_any(ds: pd.Series) -> pd.Series:
    return ds


def parse_string(ds: pd.Series) -> pd.Series:
    # pd.api.types.is_string_dtype returns True for object
    if isinstance(ds.dtype, pd.StringDtype):
        return ds
    return _coerce(ds, str).astype("string", copy=False)


def parse_number(ds: pd.Series) -> pd.Series:
    if pd.api.types.is_float_dtype(ds):
        return ds
    return _coerce(ds, float, na=float("nan"))


def parse_integer(ds: pd.Series) -> pd.Series:
    if pd.api.types.is_integer_dtype(ds):
        return ds
    return _coerce(ds, int).astype("Int64", copy=False)


def parse_boolean(
    ds: pd.Series,
    true_values: List[str] = ["true", "True", "TRUE", "1"],
    false_values: List[str] = ["false", "False", "FALSE", "0"],
) -> pd.Series:
    if pd.api.types.is_bool_dtype(ds):
        return ds
    if not isinstance(ds.dtype, pd.StringDtype):
        ds = _coerce(ds, str)
    new = pd.Series(dtype="boolean", index=ds.index)
    new[ds.isin(true_values)] = True
    new[ds.isin(false_values)] = False
    return new


def parse_date(ds: pd.Series, format: str = "default") -> pd.Series:
    if pd.api.types.is_datetime64_dtype(ds):
        return ds.dt.normalize()
    patterns = {"default": "%Y-%m-%d", "any": None}
    pattern = patterns.get(format, format)
    return pd.to_datetime(
        ds, errors="coerce", format=pattern, infer_datetime_format=pattern is None
    ).dt.normalize()


def parse_datetime(ds: pd.Series, format: str = "default") -> pd.Series:
    if pd.api.types.is_datetime64_dtype(ds):
        return ds
    patterns = {"default": "%Y-%m-%dT%H:%M:%S%z", "any": None}
    pattern = patterns.get(format, format)
    return pd.to_datetime(
        ds, errors="coerce", format=pattern, infer_datetime_format=pattern is None
    )


def parse_year(ds: pd.Series) -> pd.Series:
    return parse_integer(ds)
