from typing import Any, Dict, Iterable, Literal, Optional, Tuple, Union

from frictionless import Package
import pandas as pd
import pygsheets

from . import helpers


# ---- Constants ----

MAX_NAME_LENGTH: int = 100
"""Maximum length of sheet name (in characters)."""

# MAX_COL: int = 18278
# MAX_FORMULA_LENGTH: int = 50000
# MAX_ROWS: int = 5000000
# MAX_CELLS: int = 5000000
# MAX_FUNCTION_ARGS: float = float("inf")
# MAX_LIST_LENGTH: float = float("inf")
# MAX_MESSAGE_LENGTH: int = 50000

CONSTRAINTS: Dict[str, helpers.ConstraintCheck] = {
    **helpers.CONSTRAINTS,
    "pattern": {
        "valid": 'REGEXMATCH(TO_TEXT({col}2), "^{value}$")',
        "invalid": 'NOT(REGEXMATCH(TO_TEXT({col}2), "^{value}$"))',
        "message": "match pattern {value}",
    },
}

IN_RANGE: Dict[str, str] = {
    "valid": "ISNUMBER(ARRAYFORMULA(MATCH({search}, {range}, 0)))",
    "invalid": "ISNA(ARRAYFORMULA(MATCH({search}, {range}, 0)))",
}
"""
Formula templates to check whether a value is in a range.

* search: Cell (or value) to search for.
* range: Cell range (or value array) to search in.
"""

# ---- Helpers ----


# def get_range(
#     start: Tuple[int, int], end: Tuple[int, int], sheet: pygsheets.Worksheet
# ) -> str:
#     grange = pygsheets.GridRange(
#         propertiesjson={
#             "startRowIndex": start[0],
#             "startColumnIndex": start[1],
#             "endRowIndex": end[0],
#             "endColumnIndex": end[1],
#         },
#     )
#     grange.set_worksheet(sheet)
#     return pygsheets.DataRange(worksheet=sheet, grange=grange)

Index = Tuple[Optional[int], Optional[int]]


def protect_range(start: Index, end: Index, sheet: pygsheets.Worksheet) -> None:
    """
    Protect a cell range from accidental editing.

    Args:
        start: Start row and column index (0-based).
        end: End row and column index (0-based).
        sheet: Sheet to protect.
    """
    sheet.spreadsheet.custom_request(
        {
            "addProtectedRange": {
                "protectedRange": {
                    "range": {
                        "sheetId": sheet.id,
                        "startRowIndex": start[0],
                        "endRowIndex": end[0],
                        "startColumnIndex": start[1],
                        "endColumnIndex": end[1],
                    },
                    "warningOnly": True,
                }
            }
        },
        fields=None,
    )


def write_dataframe(
    df: pd.DataFrame,
    sheet: pygsheets.Worksheet,
    fit: Literal["cols", "rows", "both"] = None,
    freeze_header: bool = False,
    protect_header: bool = False,
    comment_header: Iterable[str] = None,
    format_header: dict = None,
    format_formulas: dict = None,
    protect_formulas: bool = False,
) -> None:
    """
    Write dataframe to Google Sheets.

    For cell formats, see the Google API reference:
    https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets/cells?hl=en#cellformat

    Args:
        df: Dataframe.
        sheet: Spreadsheet.
        fit: Whether to hide unused columns ('cols'), rows ('rows'), or both ('both').
        freeze_header: Whether to freeze the header.
        protect_header: Whether to protect the header.
        comment_header: Whether and what text to add to the header.
        format_header: Whether and how to format the header.
        format_formulas: Whether and how to format columns with formulas.
            The entire column (below the header) is formatted if the first data row
            contains a formula (i.e. starting with '=').
        protect_formulas: Whether to protect columns with formulas.
    """
    nrows, ncols = df.shape
    sheet.set_dataframe(df, start="A1", nan="", extend=True)
    # Delete unused rows and columns
    if fit in ("both", "cols"):
        sheet.resize(cols=ncols)
    if fit in ("both", "rows") and (nrows > 0 or not freeze_header):
        sheet.resize(rows=nrows + 1)
    # Format sheet
    if freeze_header:
        sheet.frozen_rows = 1
    if comment_header:
        for i, note in enumerate(comment_header, start=1):
            if note:
                pygsheets.DataRange((1, i), (1, i), sheet).apply_format(
                    cell=None, fields="note", cell_json={"note": note}
                )
    if format_header:
        pygsheets.DataRange((1, 1), (1, ncols), sheet).apply_format(
            cell=None,
            fields="userEnteredFormat",
            cell_json={"userEnteredFormat": format_header},
        )
    if protect_header:
        protect_range((0, 0), (1, ncols), sheet)
    if format_formulas and not df.empty:
        first = df.iloc[0]
        for i, value in enumerate(first, start=1):
            if isinstance(value, str) and value.startswith("="):
                pygsheets.DataRange((2, i), (sheet.rows, i), sheet).apply_format(
                    cell=None,
                    fields="userEnteredFormat",
                    cell_json={"userEnteredFormat": format_formulas},
                )
                # Protect formulas
                if protect_formulas:
                    protect_range((1, i - 1), (None, i), sheet)
    # Adjust column widths
    for i, name in enumerate(df.columns, start=1):
        width = int(round(max(10, len(name) * 1.2) * 8))
        sheet.adjust_column_width(i, i, pixel_size=width)


# ---- Template ----


class Template:
    """
    Template builder for Google Sheets.

    Different strategies are used depending on the context:

    * Formula: Automatically fill foreign key lookup columns.
    * Data validation:
        * Dropdowns: Limit columns by (first) foreign key, enum constraint,
          or boolean data type.
        * Formula: Check columns without dropdowns by data type and value constraints.
    * Conditional formatting:
        * Formula: Highlight columns by data type, constraints, and (all) foreign keys.

    Attributes:
        package: Package metadata.
        book: Google spreadsheet.
    """

    def __init__(self, package: Package, book: pygsheets.Spreadsheet) -> None:
        self.package = package
        self.book = book

    def get_columns(
        self,
        rname: str,
        fnames: Union[str, Iterable[str]],
        join: str = "|",
        first: bool = False,
        absolute: bool = False,
        indirect: bool = False,
        fixed: bool = False,
    ) -> str:
        """
        Get columns as range.

        Args:
            rname: Table (resource) name.
            fnames: Column (field) names.
            join: Whether and how to join values across multiple columns.
            first: Whether to include only the first data row.
            absolute: Whether to include the sheet name (e.g. "'Sheet1'!A2").
            indirect: Whether to wrap cell ranges in `INDIRECT` function
                (https://support.google.com/docs/answer/3093377).
            fixed: Whether to lock rows in range (e.g. "A$2").
        """
        if isinstance(fnames, str):
            fnames = [fnames]
        schema = self.package.get_resource(rname).schema
        codes = [helpers.column_code(schema.field_names.index(name)) for name in fnames]
        cells = [
            (
                (f"'{rname}'!" if absolute else "")
                + f"${code}{'$' if fixed else ''}2"
                + ("" if first else f":${code}")
            )
            for code in codes
        ]
        if indirect:
            cells = [f'INDIRECT("{cell}")' for cell in cells]
        if len(cells) == 1:
            return cells[0]
        if join is None:
            return "{" + ", ".join(cells) + "}"
        if join:
            return f' & "{join}" & '.join(cells)
        return " & ".join(cells)

    def get_sheet(self, name: str) -> pygsheets.Worksheet:
        """Get sheet by name (creating it if missing)."""
        if len(name) > MAX_NAME_LENGTH:
            raise ValueError(f"Sheet name cannot be longer than {MAX_NAME_LENGTH}")
        sheets = self.book.worksheets()
        names = [sheet.title for sheet in sheets]
        if name in names:
            return sheets[names.index(name)]
        # Rename and reuse Sheet1 if present, alone, and empty
        if (
            len(sheets) == 1
            and sheets[0].title == "Sheet1"
            and sheets[0].get_as_df().shape == (0, 0)
        ):
            sheet = sheets[0]
            sheet.title = name
            return sheet
        return self.book.add_worksheet(name)

    def get_dropdown(self, rname: str, fname: str) -> dict:
        """
        Get the dropdown-based validation for a column.

        A dropdown is returned if the column has one of the following:
        * foreign key (if multiple, only the first is used)
        * enum constraint
        * boolean data type
        """
        schema = self.package.get_resource(rname).schema
        # Foreign keys
        for key in schema.foreign_keys:
            if fname in key["fields"]:
                idx = key["fields"].index(fname)
                # NOTE: Assumes reference resource is not blank
                ref = self.package.get_resource(key["reference"]["resource"])
                ref_fname = key["reference"]["fields"][idx]
                ref_idx = ref.schema.field_names.index(ref_fname)
                col = helpers.column_code(ref_idx)
                cells = f"'{ref.name}'!${col}$2:${col}"
                return {
                    "condition_type": "ONE_OF_RANGE",
                    "condition_values": [f"={cells}"],
                    "strict": True,
                    "showCustomUi": True,
                    "inputMessage": (
                        f"Value must be in list (from {ref.name}.{ref_fname})"
                    ),
                }
        # Field constraint: enum
        field = schema.get_field(fname)
        if "enum" in field.constraints:
            return {
                "condition_type": "ONE_OF_LIST",
                "condition_values": field.constraints["enum"],
                "strict": True,
                "showCustomUi": True,
                "inputMessage": "Value must be in list",
            }
        # Field type: boolean
        if field.type == "boolean":
            return {
                "condition_type": "ONE_OF_LIST",
                "condition_values": ["TRUE", "FALSE"],
                "strict": True,
                "showCustomUi": True,
                "inputMessage": "Value must be TRUE or FALSE",
            }
        return {}

    def get_validation(self, rname: str, fname: str) -> dict:
        """
        Get the formula-based validation for a column.

        The formula checks all of the following:
        * type (number, integer, boolean, year)
        * constraints (required, unique, min/max length, min/max, pattern)
        """
        schema = self.package.get_resource(rname).schema
        field = schema.get_field(fname)
        messages = []
        formulas = []
        defaults = {
            "col": helpers.column_code(schema.field_names.index(fname)),
            "max_col": helpers.column_code(len(schema.fields) - 1),
            "ncols": len(schema.fields),
            "max_row": "",
        }
        # Field type
        if field.type in helpers.TYPES:
            info = helpers.TYPES[field.type]
            formulas.append(info["valid"].format(**defaults))
            messages.append(info["message"])
        # Field constraints
        for key, value in field.constraints.items():
            if key in CONSTRAINTS:
                info = CONSTRAINTS[key]
                formulas.append(info["valid"].format(**defaults, value=value))
                messages.append(info["message"].format(**defaults, value=value))
        if not formulas:
            return {}
        formula = helpers.merge_formulas(formulas, "AND")
        message = f"Value must be: {' and '.join(messages)}"
        return {
            "condition_type": "CUSTOM_FORMULA",
            "condition_values": [f"={formula}"],
            "strict": True,
            "inputMessage": message,
        }

    def get_condition(self, rname: str, fname: str, valid: bool = False) -> dict:
        """
        Get the formula-based conditional formatting for a column.

        The formula checks all of the following:
        * type (number, integer, boolean, year)
        * constraints (required, unique, min/max length, min/max, pattern, enum)
        * foreign keys (both simple and composite)
        """
        schema = self.package.get_resource(rname).schema
        field = schema.get_field(fname)
        formulas = {}
        col = helpers.column_code(schema.field_names.index(fname))
        defaults = {
            "col": col,
            "max_col": helpers.column_code(len(schema.fields) - 1),
            "ncols": len(schema.fields),
            "max_row": "",
        }
        f: Literal["valid", "invalid"] = "valid" if valid else "invalid"
        # Field type
        if field.type in helpers.TYPES:
            formula = helpers.TYPES[field.type][f].format(**defaults)
            formulas[formula] = True
        # Field constraints
        for key, value in field.constraints.items():
            if key in CONSTRAINTS:
                if key == "pattern":
                    # Escape any curly braces in pattern
                    value = value.replace("{", "{{").replace("}", "}}")
                info = CONSTRAINTS[key]
                formula = info[f].format(**defaults, value=value)
                formulas[formula] = info.get("ignore_blank", True)
            elif key == "enum":
                enums = ", ".join([helpers.format_value(x) for x in value])
                # Escape curly braces
                formula = IN_RANGE[f].format(search=f"{col}2", range=f"{{{{{enums}}}}}")
                formulas[formula] = True
        # Foreign keys
        for key in schema.foreign_keys:
            if fname in key["fields"]:
                search = self.get_columns(rname, key["fields"], first=True)
                cells = self.get_columns(
                    key["reference"]["resource"],
                    key["reference"]["fields"],
                    absolute=True,
                    indirect=True,
                    fixed=True,
                )
                formula = IN_RANGE[f].format(search=search, range=cells)
                formulas[formula] = True
        if not formulas:
            return {}
        formula = helpers.merge_conditions(
            list(formulas.keys()),
            valid=valid,
            ignore_blanks=[formula for formula in formulas if formulas[formula]],
        ).format(col=col)
        return {"condition_type": "CUSTOM_FORMULA", "condition_values": [f"={formula}"]}

    def to_dataframes(self) -> Dict[str, pd.DataFrame]:
        """
        Build a dataframe of values for each sheet.

        Includes:
        * Header row with column names
        * Formula for each lookup column (with both simple and composite foreign keys)
        """
        dfs = {
            resource.name: pd.DataFrame(
                columns=resource.schema.field_names,
                data=[[pd.NA] * len(resource.schema.fields)],
            )
            for resource in self.package.resources
        }
        # Add foreign key lookups
        for resource in self.package.resources:
            for key in resource.schema.foreign_keys:
                if len(key["fields"]) > 1:
                    template = (
                        "=ARRAYFORMULA(IFERROR(VLOOKUP("
                        + "{search}, {{{range}, {lookup}}}, 2, 0)))"
                    )
                else:
                    template = (
                        '=ARRAYFORMULA(IF(ISBLANK({search}), "", VLOOKUP('
                        + "{search}, {{{range}, {lookup}}}, 2, 0)))"
                    )
                for fname in key.get("lookup", {}):
                    formula = template.format(
                        search=self.get_columns(
                            resource.name, key["fields"], fixed=True
                        ),
                        range=self.get_columns(
                            key["reference"]["resource"],
                            key["reference"]["fields"],
                            fixed=True,
                            join="|",
                            absolute=True,
                        ),
                        lookup=self.get_columns(
                            key["reference"]["resource"],
                            key["lookup"][fname],
                            fixed=True,
                            absolute=True,
                        ),
                    )
                    dfs[resource.name][fname][0] = formula
        return dfs

    def write_dataframes(
        self,
        dfs: Dict[str, pd.DataFrame],
        comment_headers: Dict[str, Iterable[str]] = None,
        **kwargs: Any,
    ) -> None:
        """
        Write dataframes to sheets.

        Args:
            dfs: Dataframes.
            comment_headers: Whether and what text to add to the header of each table.
            kwargs: Optional arguments to :func:`write_dataframe`.
        """
        for name, df in dfs.items():
            sheet = self.get_sheet(name)
            self.book.client.set_batch_mode(True)
            comment_header = None
            if comment_headers and name in comment_headers:
                comment_header = comment_headers[name]
            write_dataframe(df, sheet=sheet, comment_header=comment_header, **kwargs)
            self.book.client.run_batch()
            self.book.client.set_batch_mode(False)

    def reset(self) -> None:
        """Reset template by removing all sheets with table names."""
        sheets = self.book.worksheets()
        n = len(sheets)
        for sheet in sheets:
            if sheet.title in self.package.resource_names:
                if n == 1:
                    # Reset to single blank worksheet 'Sheet1'
                    self.book.add_worksheet("Sheet1")
                self.book.del_worksheet(sheet)
                n -= 1

    def build(
        self, valid: bool = False, format_condition: dict = None, **kwargs: Any
    ) -> None:
        """
        Build template.

        Args:
            valid: Whether condition is True if value is valid.
            format_condition: Whether and how to format cells meeting the condition.
            kwargs: Optional arguments to :meth:`write_dataframes`.
        """
        # Write dataframes
        dfs = self.to_dataframes()
        self.write_dataframes(dfs, **kwargs)
        # Add validation
        self.book.client.set_batch_mode(True)
        for resource in self.package.resources:
            sheet = self.get_sheet(resource.name)
            for i, field in enumerate(resource.schema.fields, start=1):
                start, end = (2, i), (sheet.rows, i)
                # Validation
                validation = self.get_dropdown(resource.name, field.name)
                if not validation:
                    validation = self.get_validation(resource.name, field.name)
                if validation:
                    sheet.set_data_validation(start, end, **validation)
                # Conditional formatting
                if format_condition:
                    condition = self.get_condition(
                        resource.name, field.name, valid=valid
                    )
                    if condition:
                        sheet.add_conditional_formatting(
                            start, end, format=format_condition, **condition
                        )
        self.book.client.run_batch()
        self.book.client.set_batch_mode(False)
