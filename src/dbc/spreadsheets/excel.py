from pathlib import Path
from typing import Any, Dict, Iterable, Literal, Sequence, Union

from frictionless import Package
import pandas as pd
import xlsxwriter
import xlsxwriter.format
import xlsxwriter.worksheet

from . import helpers


# ---- Constants ----

MAX_NAME_LENGTH: int = 31
"""Maximum length of sheet name (in characters)."""

MAX_COL: int = 16384
"""Maximum number of columns per sheet."""

MAX_ROW: int = 1048576
"""Maximum number of rows per sheet."""

MAX_LIST_LENGTH: int = 255
"""Maximum length of inline dropdown (characters of comma-separated list)."""

# MAX_FORMULA_LENGTH: int = 8192
# MAX_FUNCTION_ARGS: int = 30
# MAX_TITLE_LENGTH: int = 32
# MAX_MESSAGE_LENGTH: int = 255

IN_RANGE: Dict[str, str] = {
    "valid": "ISNUMBER(MATCH({search}, {range}, 0))",
    "invalid": "ISNA(MATCH({search}, {range}, 0))",
}
"""
Formula templates to check whether a value is in a range.

* search: Cell (or value) to search for.
* range: Cell range (or value array) to search in.
"""

# ---- Helpers ----


def write_dataframe(
    df: pd.DataFrame,
    sheet: xlsxwriter.worksheet.Worksheet,
    fit: bool = True,
    freeze_header: bool = False,
    comment_header: Sequence[str] = None,
    format_header: xlsxwriter.format.Format = None,
    format_formulas: xlsxwriter.format.Format = None,
    format_comments: dict = None,
    format_default: xlsxwriter.format.Format = None,
) -> None:
    """
    Write dataframe to Microsoft Excel.

    Args:
        df: Dataframe.
        sheet: Spreadsheet.
        fit: Whether to hide unused columns. Ignored if `comment_header`,
            since Excel prevents hiding columns that overlap a comment.
        freeze_header: Whether to freeze the header.
        comment_header: Whether and what text to add to the header.
        format_header: Whether and how to format the header.
        format_formulas: Whether and how to format columns with formulas.
            The entire column (below the header) is formatted if the first data row
            contains a formula (i.e. starting with '=').
        format_comments: Whether and how to format header comments.
        format_default: Whether and how to format regular and unused cells.
    """
    ncols = len(df.columns)
    # Write and format header row
    sheet.write_row(0, 0, list(df.columns), format_header)
    # Write data rows
    for i, row in enumerate(df.fillna("").itertuples(index=False, name=None), start=1):
        sheet.write_row(i, 0, row)
    # Format and hide unused columns
    options = None
    if fit and not comment_header:
        options = {"hidden": 1}
    sheet.set_column(ncols, MAX_COL - 1, cell_format=format_default, options=options)
    # Freeze panes
    if freeze_header:
        sheet.freeze_panes(1, 0)
    # Add comments
    if comment_header:
        for i, comment in enumerate(comment_header):
            sheet.write_comment(0, i, comment, options=format_comments)
    # Format used columns
    for i, name in enumerate(df.columns):
        cell_format = format_default
        if (
            format_formulas
            and not df.empty
            and isinstance(df[name].iloc[0], str)
            and df[name].startswith("=")
        ):
            cell_format = format_formulas
        # Resize columns and add header comments
        # Can apply to full column since header (row) format takes precendence
        sheet.set_column(i, i, width=max(10, len(name) * 1.2), cell_format=cell_format)


# ---- Template ----


class Template:
    """
    Template builder for Microsoft Excel.

    Different strategies are used depending on the context:

    * Data validation:
        * Dropdowns: Limit columns by (first) foreign key or foreign key lookup,
          enum constraint, or boolean data type.
        * Formula: Check columns without dropdowns by data type and value constraints.
    * Conditional formatting:
        * Formula: Highlight columns by data type, constraints, and (all) foreign keys
          and foreign key lookups.

    Conditional formatting of foreign key lookups and composite foreign keys is slow
    may be disabled by leaving these out of :attr:`package`.

    Due to the absence of array lookup formulas in older versions of Microsoft Excel,
    formulas to automatically fill foreign key lookup columns would require a formula in
    each cell of the column and was thus left out.

    Attributes:
        package: Package metadata.
        book: Google spreadsheet.
        enums: Cache of enum constraints, where the key is the enum as a tuple
            (e.g. `(1, 2)`) and the value is the range where they are stored
            (e.g. `enum!$A$1:$A$2`).
    """

    def __init__(self, package: Package, path: Union[str, Path]) -> None:
        self.package = package
        self.book: xlsxwriter.Workbook = xlsxwriter.Workbook(path)
        self.enums: Dict[tuple, str] = {}

    def get_columns(
        self,
        rname: str,
        fnames: Union[str, Iterable[str]],
        join: str = "|",
        first: bool = False,
        absolute: bool = False,
        fixed: bool = False,
    ) -> str:
        """
        Get columns as range.

        Args:
            rname: Table (resource) name.
            fnames: Column (field) names.
            join: How to join values across multiple columns.
            first: Whether to include only the first data row.
            absolute: Whether to include the sheet name (e.g. "'Sheet1'!A2").
            fixed: Whether to lock rows in range (e.g. "A$2").
        """
        if isinstance(fnames, str):
            fnames = [fnames]
        schema = self.package.get_resource(rname).schema
        codes = [helpers.column_code(schema.field_names.index(name)) for name in fnames]
        cells = [
            (
                (f"'{rname}'!" if absolute else "")
                + f"${code}{'$' if fixed else ''}2"
                + ("" if first else f":${code}{'$' if fixed else ''}{MAX_ROW}")
            )
            for code in codes
        ]
        if len(cells) == 1:
            return cells[0]
        if join:
            return f' & "{join}" & '.join(cells)
        return " & ".join(cells)

    def get_sheet(self, name: str) -> xlsxwriter.worksheet.Worksheet:
        """Get sheet by name (creating it if missing)."""
        if len(name) > MAX_NAME_LENGTH:
            raise ValueError(f"Sheet name cannot be longer than {MAX_NAME_LENGTH}")
        sheet = self.book.get_worksheet_by_name(name)
        return sheet or self.book.add_worksheet(name)

    def _get_enum_range(self, values: Iterable, name: str) -> str:
        """Get the range for an enum constraint."""
        values = tuple(values)
        if values in self.enums:
            name = self.enums[values]
        else:
            # Write enum
            sheet = self.get_sheet(name)
            sheet.write_column(0, 0, values)
            sheet.protect()
            sheet.hide()
            self.enums[values] = name
        return f"'{name}'!$A$1:$A${len(values)}"

    def _get_enum_dropdown(self, values: Iterable, name: str) -> dict:
        """Get the dropdown for an enum constraint."""
        # txt = ",".join([str(value) for value in values])
        # if len(txt) <= MAX_LIST_LENGTH:
        #     return {
        #         "validate": "list",
        #         "value": values,
        #         "error_title": "Invalid value",
        #         "error_message": "Value must be in list",
        #     }
        cells = self._get_enum_range(values, name)
        return {
            "validate": "list",
            "value": cells,
            "error_title": "Invalid value",
            "error_message": "Value must be in list",
        }

    def get_dropdown(self, rname: str, fname: str) -> dict:
        """
        Get the dropdown-based validation for a column.

        A dropdown is returned if the column has one of the following:
        * foreign key or foreign key lookup (if multiple, only the first is used)
        * enum constraint
        * boolean data type
        """
        schema = self.package.get_resource(rname).schema
        # Foreign keys
        for key in schema.foreign_keys:
            if fname not in key["fields"] and fname not in key.get("lookup", {}):
                continue
            # NOTE: Assumes reference resource is not blank
            ref = self.package.get_resource(key["reference"]["resource"])
            if fname in key["fields"]:
                idx = key["fields"].index(fname)
                ref_fname = key["reference"]["fields"][idx]
                ref_idx = ref.schema.field_names.index(ref_fname)
            else:
                ref_fname = key["lookup"][fname]
                ref_idx = ref.schema.field_names.index(ref_fname)
            col = helpers.column_code(ref_idx)
            cells = f"'{ref.name}'!${col}$2:${col}${MAX_COL}"
            return {
                "validate": "list",
                "value": cells,
                "error_title": "Invalid value",
                "error_message": f"Value must be in list (from {ref.name}.{ref_fname})",
            }
        # Field constraint: enum
        field = schema.get_field(fname)
        if "enum" in field.constraints:
            return self._get_enum_dropdown(
                field.constraints["enum"], f"enum.{field.name}"
            )
        # Field type: boolean
        if field.type == "boolean":
            return {
                "validate": "list",
                "value": ["TRUE", "FALSE"],
                "error_title": "Invalid value",
                "error_message": "Value must be TRUE or FALSE",
            }
        return {}

    def get_validation(self, rname: str, fname: str) -> dict:
        """
        Get the formula-based validation for a column.

        The formula checks all of the following:
        * type (number, integer, boolean, year)
        * constraints (required, unique, min/max length, min/max)
        """
        schema = self.package.get_resource(rname).schema
        field = schema.get_field(fname)
        messages = []
        formulas = []
        ignore_blank = True
        defaults = {
            "col": helpers.column_code(schema.field_names.index(fname)),
            "max_col": helpers.column_code(len(schema.fields) - 1),
            "ncols": len(schema.fields),
            "max_row": f"${MAX_ROW}",
        }
        # Field type
        if field.type in helpers.TYPES:
            info = helpers.TYPES[field.type]
            formulas.append(info["valid"].format(**defaults))
            messages.append(info["message"])
        # Field constraints
        for key, value in field.constraints.items():
            if key in helpers.CONSTRAINTS:
                info = helpers.CONSTRAINTS[key]
                formulas.append(info["valid"].format(**defaults, value=value))
                messages.append(info["message"].format(**defaults, value=value))
                if not info.get("ignore_blank", True):
                    ignore_blank = False
        if not formulas:
            return {}
        formula = helpers.merge_formulas(formulas, "AND")
        message = "Value must be:\n" + "\n".join(messages)
        return {
            "validate": "custom",
            # Reduce length of formula
            "value": formula.replace(" ", ""),
            "error_title": "Invalid value",
            "error_message": message,
            "ignore_blank": ignore_blank,
        }

    def get_condition(self, rname: str, fname: str, valid: bool = False) -> dict:
        """
        Get the formula-based conditional formatting for a column.

        The formula checks all of the following:
        * type (number, integer, boolean, year)
        * constraints (required, unique, min/max length, min/max, enum)
        * foreign keys (both simple and composite)
        * foreign key lookups
        """
        schema = self.package.get_resource(rname).schema
        field = schema.get_field(fname)
        formulas = {}
        col = helpers.column_code(schema.field_names.index(fname))
        defaults = {
            "col": col,
            "max_col": helpers.column_code(len(schema.fields) - 1),
            "ncols": len(schema.fields),
            "max_row": f"${MAX_ROW}",
        }
        f: Literal["valid", "invalid"] = "valid" if valid else "invalid"
        # Field type
        if field.type in helpers.TYPES:
            formula = helpers.TYPES[field.type][f].format(**defaults)
            formulas[formula] = True
        # Field constraints
        for key, value in field.constraints.items():
            if key in helpers.CONSTRAINTS:
                info = helpers.CONSTRAINTS[key]
                formula = info[f].format(**defaults, value=value)
                formulas[formula] = info.get("ignore_blank", True)
            elif key == "enum":
                enum_range = self._get_enum_range(
                    field.constraints["enum"], f"enum.{field.name}"
                )
                formula = IN_RANGE[f].format(search=f"{col}2", range=enum_range)
                formulas[formula] = True
        # Foreign keys
        for key in schema.foreign_keys:
            if fname not in key["fields"] and fname not in key.get("lookup", {}):
                continue
            search = self.get_columns(rname, key["fields"], first=True)
            cells = self.get_columns(
                key["reference"]["resource"],
                key["reference"]["fields"],
                absolute=True,
                fixed=True,
            )
            if fname in key["fields"]:
                formula = IN_RANGE[f].format(search=search, range=cells)
                formulas[formula] = True
            else:
                search = self.get_columns(rname, key["fields"] + [fname], first=True)
                cells = self.get_columns(
                    key["reference"]["resource"],
                    key["reference"]["fields"] + [key["lookup"][fname]],
                    absolute=True,
                    fixed=True,
                )
                formula = IN_RANGE[f].format(search=search, range=cells)
                formulas[formula] = True
        if not formulas:
            return {}
        formula = helpers.merge_conditions(
            list(formulas.keys()),
            valid=valid,
            ignore_blanks=[formula for formula in formulas if formulas[formula]],
        ).format(col=col)
        return {
            "type": "formula",
            # Reduce length of formula
            "criteria": formula.replace(" ", ""),
        }

    def to_dataframes(self) -> Dict[str, pd.DataFrame]:
        """
        Build a dataframe of values for each table.

        Includes:
        * Header row with column names
        """
        return {
            resource.name: pd.DataFrame(columns=resource.schema.field_names)
            for resource in self.package.resources
        }

    def write_dataframes(
        self,
        dfs: Dict[str, pd.DataFrame],
        comment_headers: Dict[str, Sequence[str]] = None,
        **kwargs: Any,
    ) -> None:
        """
        Write dataframes to sheets.

        Args:
            dfs: Dataframes.
            comment_headers: Whether and what text to add to the header of each table.
            kwargs: Optional arguments to :func:`write_dataframe`.
                Any `xlsxwriter.format.Format` can be provided as a `dict`.
        """
        # Register formats
        for k, v in kwargs.items():
            if k in ("format_default", "format_header", "format_formulas"):
                if isinstance(v, dict):
                    kwargs[k] = self.book.add_format(v)
        for name, df in dfs.items():
            sheet = self.get_sheet(name)
            comment_header = None
            if comment_headers and name in comment_headers:
                comment_header = comment_headers[name]
            write_dataframe(df, sheet=sheet, comment_header=comment_header, **kwargs)

    def build(
        self, valid: bool = False, format_condition: dict = None, **kwargs: Any
    ) -> None:
        """
        Build template.

        Args:
            valid: Whether condition is True if value is valid.
            format_condition: Whether and how to format cells meeting the condition.
            kwargs: Optional arguments to :meth:`write_dataframes`.
        """
        # Write dataframes
        dfs = self.to_dataframes()
        self.write_dataframes(dfs, **kwargs)
        # Add validation
        if format_condition:
            format_condition = self.book.add_format(format_condition)
        for resource in self.package.resources:
            sheet = self.get_sheet(resource.name)
            for field in resource.schema.fields:
                cells = self.get_columns(resource.name, field.name)
                # Validation
                validation = self.get_dropdown(resource.name, field.name)
                if not validation:
                    validation = self.get_validation(resource.name, field.name)
                if validation:
                    sheet.data_validation(cells, validation)
                # Conditional formatting
                if format_condition:
                    condition = self.get_condition(
                        resource.name, field.name, valid=valid
                    )
                    if condition:
                        sheet.conditional_format(
                            cells, {**condition, "format": format_condition}
                        )
        self.book.close()
