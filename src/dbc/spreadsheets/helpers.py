from functools import reduce
from typing import Any, Dict, Iterable, List, Literal, Sequence, TypedDict


class Check(TypedDict):
    """Information used to check a column."""

    valid: str
    """Formula returning true if valid."""
    invalid: str
    """Formula returning true if invalid."""
    message: str
    """Message describing the check."""


TYPES: Dict[str, Check] = {
    "number": {
        "valid": "ISNUMBER({col}2)",
        "invalid": "NOT(ISNUMBER({col}2))",
        "message": "number",
    },
    "integer": {
        "valid": "IF(ISNUMBER({col}2), INT({col}2) = {col}2, FALSE)",
        "invalid": "IF(ISNUMBER({col}2), INT({col}2) <> {col}2, TRUE)",
        "message": "integer",
    },
    "year": {
        "valid": "IF(ISNUMBER({col}2), INT({col}2) = {col}2, FALSE)",
        "invalid": "IF(ISNUMBER({col}2), INT({col}2) <> {col}2, TRUE)",
        "message": "year",
    },
    "boolean": {
        "valid": "OR({col}2 = TRUE, {col}2 = FALSE)",
        "invalid": "AND({col}2 <> TRUE, {col}2 <> FALSE)",
        "message": "TRUE or FALSE",
    },
    # 'date': {
    #   # NOTE: Only works for dates >= 1904
    #   'valid': 'NOT(ISERROR(DATEVALUE({col}2)))',
    #   'message': 'date'
    # }
}
"""
Formula templates for (in)valid type checks.

* col: Column code.
"""


class ConstraintCheck(Check, total=False):
    """Information used to check a column constraint."""

    ignore_blank: bool
    """Whether to skip the checking of blank cells."""


CONSTRAINTS: Dict[str, ConstraintCheck] = {
    "required": {
        "valid": "NOT(ISBLANK({col}2))",
        "invalid": "AND(ISBLANK({col}2), COUNTBLANK($A2:${max_col}2) <> {ncols})",
        "message": "not blank",
        "ignore_blank": False,
    },
    "unique": {
        "valid": "COUNTIF({col}$2:{col}{max_row}, {col}2) < 2",
        "invalid": "COUNTIF({col}$2:{col}{max_row}, {col}2) >= 2",
        "message": "unique",
    },
    "minLength": {
        "valid": "LEN({col}2) >= {value}",
        "invalid": "LEN({col}2) < {value}",
        "message": "length ≥ {value}",
    },
    "maxLength": {
        "valid": "LEN({col}2) <= {value}",
        "invalid": "LEN({col}2) > {value}",
        "message": "length ≤ {value}",
    },
    "minimum": {
        "valid": "{col}2 >= {value}",
        "invalid": "{col}2 < {value}",
        "message": "≥ {value}",
    },
    "maximum": {
        "valid": "{col}2 <= {value}",
        "invalid": "{col}2 > {value}",
        "message": "≤ {value}",
    },
}
"""
Formula templates for (in)valid constraint checks.

* col: Column code.
* value: Constraint value.
* max_col: Column code of last column in table.
* ncols: Number of columns in table.
* max_row: Last row index (optional), prefixed as needed by '$'.
"""

LETTERS: str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"""Letters of the latin alphabet."""


def column_code(i: int) -> str:
    """
    Convert a column index to a spreadsheet column code.

    Examples:
        >>> column_code(0)
        'A'
        >>> column_code(26)
        'AA'
        >>> column_code(16383)
        'XFD'
    """
    letters: List[str] = []
    i = i + 1
    while i:
        i, remainder = divmod(i - 1, 26)
        letters[:0] = LETTERS[remainder]
    return "".join(letters)


def column_index(code: str) -> int:
    """
    Convert a spreadsheet column code to a column index.

    Examples:
        >>> column_index('A')
        0
        >>> column_index('AA')
        26
        >>> column_index('XFD')
        16383
    """
    # https://gist.github.com/dbspringer/643254008e6784aa749e#file-col2num-py
    return reduce(lambda x, y: x * 26 + y, [ord(c) - ord("A") + 1 for c in code]) - 1


def format_value(x: Any) -> str:
    """
    Format a singleton value for spreadsheet cells or formulas.

    Examples:
        >>> format_value(True)
        'TRUE'
        >>> format_value(1)
        '1'
        >>> format_value('a')
        '"a"'
        >>> format_value(None)
        ''
    """
    if isinstance(x, bool):
        return str(x).upper()
    if isinstance(x, (int, float)):
        return str(x)
    if isinstance(x, str):
        return f'"{x}"'
    if x is None:
        return ""
    raise ValueError(f"Unexpected value {x} of type {type(x)}")


def merge_formulas(formulas: Sequence[str], operator: Literal["AND", "OR"]) -> str:
    """
    Merge formulas by a logical operator.

    Examples:
        >>> merge_formulas(['A2 > 0', 'A2 < 3'], 'AND')
        'AND(A2 > 0, A2 < 3)'
    """
    if not formulas:
        return ""
    if len(formulas) == 1:
        return formulas[0]
    return f"{operator}({', '.join(formulas)})"


def merge_conditions(
    formulas: Iterable[str], valid: bool, ignore_blanks: Sequence[str] = None
) -> str:
    """
    Merge conditions.

    Args:
        formulas: Formulas returning TRUE or FALSE.
        valid: Whether formulas return TRUE for valid values.
        ignore_blanks: Formulas that should ignore blanks
            (by returning either TRUE or FALSE, depending on `valid`).

    Returns:
        Merged formula with (if `ignore_blanks`) a column code placeholder (`{col}`).

    Examples:
        >>> formulas = ['A2 > 0', 'A2 < 3']
        >>> merge_conditions(formulas, True)
        'AND(A2 > 0, A2 < 3)'
        >>> merge_conditions(formulas, True, ignore_blanks=formulas)
        'IF(ISBLANK({col}2), TRUE, AND(A2 > 0, A2 < 3))'
        >>> merge_conditions(formulas, True, ignore_blanks=formulas[1:])
        'AND(A2 > 0, IF(ISBLANK({col}2), TRUE, A2 < 3))'
        >>> merge_conditions(formulas, False, ignore_blanks=formulas[1:])
        'OR(A2 > 0, IF(ISBLANK({col}2), FALSE, A2 < 3))'
    """
    operator: Literal["AND", "OR"] = "AND" if valid else "OR"
    fs = [
        formula
        for formula in formulas
        if not ignore_blanks or formula not in ignore_blanks
    ]
    if ignore_blanks:
        # Wrap formulas ignoring blank in single if statement
        merged = merge_formulas(ignore_blanks, operator=operator)
        fs.append(f"IF(ISBLANK({{col}}2), {format_value(valid)}, {merged})")
    return merge_formulas(fs, operator=operator)


# def clip_string(s: str, max_length: int, ellipsis: str = None) -> str:
#     """
#     Clip string to a maximum length.

#     Any trailing whitespace resulting from the clipping is stripped.

#     Args:
#         s: String to clip.
#         max_length: Maximum string length in characters.
#         ellipsis: Whether and what string to insert if the string is clipped.

#     Examples:
#         >>> clip_string('12345678', max_length=7)
#         '1234567'
#         >>> clip_string('12345678', max_length=7, ellipsis='...')
#         '1234...'
#         >>> clip_string('123 45678', max_length=7, ellipsis='...')
#         '123...'
#     """
#     if len(s) <= max_length:
#         return s
#     if not ellipsis:
#         return s[:max_length].rstrip()
#     if len(ellipsis) > max_length:
#         raise ValueError("Ellipsis cannot be longer than max_length")
#     return s[: (max_length - len(ellipsis))].rstrip() + ellipsis
