from collections import defaultdict
import os
from pathlib import Path
from typing import List, Union

from ccorp.ruamel.yaml.include import YAML
from frictionless import Schema, Package
import jinja2
import pandas as pd


def read_yaml(
    path: str, encoding: str = "utf-8", **kwargs
) -> Union[dict, list, int, float, str, bool]:
    yaml = YAML()
    return yaml.load(open(path, mode="r", encoding=encoding, **kwargs))


def write_file_path(path: Union[str, Path]) -> None:
    path = Path(path)
    if path.is_dir():
        raise ValueError(f"Path is a directory: {path}")
    path.parent.mkdir(parents=True, exist_ok=True)


def expand_schema(schema: Schema, unique_keys: List[List[str]] = None) -> None:
    """
    Examples:
        >>> schema = Schema({
        ...     'fields': [
        ...         {'name': 'x', 'constraints': {'required': True}},
        ...         {'name': 'y'}
        ...     ],
        ...     'primaryKey': ['x', 'y'],
        ...     'foreignKeys': [{
        ...         'fields': ['y'],
        ...         'reference': {'resource': '', 'fields': ['x']}
        ...     }]
        ... })
        >>> expand_schema(schema)
        >>> schema.fields[0]
        {'name': 'x', 'constraints': {'required': True, 'unique': True}}
        >>> schema.fields[1]
        {'name': 'y', 'constraints': {'required': True}}
    """
    if not unique_keys:
        unique_keys = []
    required, unique = [], []
    # foreign_key.reference: Add to unique_keys (if self-reference)
    for foreign_key in schema.foreign_keys:
        if not foreign_key["reference"]["resource"]:
            fields = foreign_key["reference"]["fields"]
            if fields not in unique_keys:
                unique_keys.append(fields)
    # primary_key: Add to field.constraints.required and unique_keys
    if schema.primary_key:
        fields = schema.primary_key
        required.extend(fields)
        if fields not in unique_keys:
            unique_keys.append(fields)
    # unique_key: Add to field.constraints.unique (if single)
    for fields in unique_keys:
        if len(fields) == 1:
            unique.extend(fields)
    # Update field constraints
    for field in schema.fields:
        if field.name in required:
            field.constraints["required"] = True
        if field.name in unique:
            field.constraints["unique"] = True


def expand_package(package: Package) -> None:
    # foreign_key.reference.fields: Add to reference.unique_keys
    unique_keys = defaultdict(list)
    for resource in package.resources:
        for foreign_key in resource.schema.foreign_keys:
            name = foreign_key["reference"]["resource"]
            if name in package.resource_names:
                unique_keys[name].append(foreign_key["reference"]["fields"])
    for resource in package.resources:
        expand_schema(resource.schema, unique_keys=unique_keys[resource.name])


def filter_dict(
    x: dict, include: list = None, exclude: list = None, order: list = None
) -> dict:
    """Filter and order dictionary by key names."""
    if include:
        x = {key: x[key] for key in x if key in include}
    if exclude:
        x = {key: x[key] for key in x if key not in exclude}
    if order:
        index = [
            (order.index(key) if key in order else len(order), i)
            for i, key in enumerate(x)
        ]
        sorted_keys = [key for _, key in sorted(zip(index, x.keys()))]
        x = {key: x[key] for key in sorted_keys}
    return x


def json_to_markdown(
    x: Union[dict, list, int, float, str, bool],
    level: int = 0,
    tab: int = 2,
    flatten_scalar_lists: bool = True,
) -> str:
    """Render any JSON-like object as Markdown, using nested bulleted lists."""

    def _scalar_list(x) -> bool:
        return isinstance(x, list) and all(not isinstance(xi, (dict, list)) for xi in x)

    def _iter(x: Union[dict, list, int, float, str, bool], level: int = 0) -> str:
        if isinstance(x, (dict, list)):
            if isinstance(x, dict):
                labels = [f"- `{key}`" for key in x]
            elif isinstance(x, list):
                labels = [f"- [{i + 1}]" for i in range(len(x))]
            values = x if isinstance(x, list) else list(x.values())
            if isinstance(x, list) and flatten_scalar_lists:
                scalar = [not isinstance(value, (dict, list)) for value in values]
                if all(scalar):
                    values = [f"{values}"]
            lines = []
            for label, value in zip(labels, values):
                if isinstance(value, (dict, list)) and (
                    not flatten_scalar_lists or not _scalar_list(value)
                ):
                    lines.append(f"{label}\n{_iter(value, level=level + 1)}")
                else:
                    if isinstance(value, str):
                        # Indent to align following lines with '- '
                        value = jinja2.filters.do_indent(value, width=2, first=False)
                    lines.append(f"{label} {value}")
            txt = "\n".join(lines)
        else:
            txt = str(x)
        if level > 0:
            txt = jinja2.filters.do_indent(txt, width=tab, first=True, blank=False)
        return txt

    return jinja2.filters.do_indent(
        _iter(x, level=0), width=tab * level, first=True, blank=False
    )


def dicts_to_markdown_table(dicts: List[dict], **kwargs) -> str:
    """Tabulate dictionaries and render as a Markdown table."""
    if kwargs:
        dicts = [filter_dict(x, **kwargs) for x in dicts]
    df = pd.DataFrame(dicts)
    return df.where(df.notnull(), None).to_markdown(index=False)


# ---- Jinja2 Templates ----


class RelativeEnvironment(jinja2.Environment):
    """Override join_path() to enable relative template paths."""

    def join_path(self, template, parent):
        return os.path.normpath(os.path.join(os.path.dirname(parent), template))


def render_template(path: str, data: dict, encoding: str = "utf-8") -> str:
    dir = os.path.dirname(os.path.abspath(path))
    file = os.path.split(path)[1]
    environment = RelativeEnvironment(
        loader=jinja2.FileSystemLoader(dir, encoding=encoding),
        lstrip_blocks=True,
        trim_blocks=True,
    )
    environment.filters["filter_dict"] = filter_dict
    environment.filters["dict_to_markdown"] = json_to_markdown
    environment.filters["tabulate"] = dicts_to_markdown_table
    template = environment.get_template(file)
    return template.render(**data)
