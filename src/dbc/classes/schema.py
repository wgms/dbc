from io import StringIO
from os import PathLike
from typing import Any, Iterable, List, Optional, TextIO, Tuple, Union

import frictionless
from frictionless.metadata import Metadata
from frictionless.plugins.csv import CsvDialect
import pandas as pd

from .field import Field

Path = Union[str, PathLike, StringIO, TextIO]


class Schema(frictionless.Schema):
    def __init__(
        self,
        descriptor: Union[str, dict] = None,
        *args: Any,
        required: bool = True,
        **kwargs: Any
    ) -> None:
        super().__init__(descriptor=descriptor, *args, **kwargs)
        self.fields: List[Field] = [Field(field) for field in self.fields]
        self.required = required

    @Metadata.property
    def required(self):
        return self.get("required")

    def read_csv(
        self,
        paths: Union[Path, Iterable[Path]],
        dialect: CsvDialect = None,
        encoding: str = "utf-8",
        parse_dtypes: bool = None,
        parse_dates: bool = False,
        read_all_columns: bool = False,
    ) -> pd.DataFrame:
        if not dialect:
            dialect = CsvDialect()
        missing_values = set(self.missing_values)
        if dialect.null_sequence:
            missing_values.add(dialect.null_sequence)
        dtype: Optional[Union[dict, str]]
        if parse_dtypes is None:
            dtype = None
        elif parse_dtypes:
            dtype = {
                field.name: field.dtype
                for field in self.fields
                if field.type not in ("date", "datetime")
            }
        else:
            dtype = "object"
        dates: list = []
        if parse_dates:
            dates = [
                field.name
                for field in self.fields
                if field.type in ("date", "datetime")
            ]
        kwargs = {
            "header": 0,
            "names": None,
            "index_col": False,
            "squeeze": False,
            "dtype": dtype,
            "parse_dates": dates,
            "infer_datetime_format": True,
            "engine": "c",
            "na_values": missing_values,
            "keep_default_na": False,
            "na_filter": True,
            "skip_blank_lines": False,
            "comment": dialect.comment_char,
            "encoding": encoding,
            "dialect": dialect.to_python(),
            "on_bad_lines": "error",
            "low_memory": True,
            "usecols": None if read_all_columns else lambda x: x in self.field_names,
        }
        dfs = []
        if isinstance(paths, (str, PathLike, StringIO, TextIO)):
            paths = [paths]
        for path in paths:
            dfs.append(pd.read_csv(path, **kwargs))
        return pd.concat(dfs, ignore_index=True)

    def parse(
        self, df: pd.DataFrame, inplace: bool = False
    ) -> Tuple[pd.DataFrame, pd.DataFrame]:
        parsed = df if inplace else df.copy()
        reports = []
        for name in df:
            if name in self.field_names:
                field = self.get_field(name)
                parsed[name], failures = field.parse(df[name])
                reports.append(pd.DataFrame({"field": name, "value": failures}))
        if reports:
            report = pd.concat(reports)
        else:
            report = pd.DataFrame(columns=["field", "value"], index=df.index[0:0])
        return parsed, report

    def reindex(self, df: pd.DataFrame, inplace: bool = False) -> pd.DataFrame:
        new = [name for name in self.field_names if name not in df]
        return df.reindex(columns=self.field_names, copy=not inplace).astype(
            {name: self.get_field(name).dtype for name in new}, copy=False
        )
