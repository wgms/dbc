from typing import Any, Callable, Tuple, Union

import frictionless
import pandas as pd

from .. import parsers


class Field(frictionless.Field):
    def __init__(
        self, descriptor: Union[str, dict] = None, *args: Any, **kwargs: Any
    ) -> None:
        super().__init__(descriptor=descriptor, *args, **kwargs)

    @property
    def dtype(self) -> Union[str, pd.CategoricalDtype]:
        """Pandas data type."""
        if "enum" in self.constraints:
            return pd.CategoricalDtype(self.constraints["enum"])
        dtypes = {
            "string": "string",
            "number": "Float64",
            "integer": "Int64",
            "boolean": "boolean",
            "date": "datetime64[ns]",
            "datetime": "datetime64[ns]",
            "year": "Int64",
        }
        return dtypes.get(self.type, "object")

    @property
    def fast_dtype(self) -> Union[str, pd.CategoricalDtype]:
        """Pandas data type."""
        if "enum" in self.constraints:
            return pd.CategoricalDtype(self.constraints["enum"])
        if self.type in ("integer", "year"):
            if self.constraints.get("required", False):
                return "int64"
            return "Int64"
        if self.type == "boolean":
            if self.constraints.get("required", False):
                return "bool"
            return "boolean"
        dtypes = {
            "string": "str",
            "number": "float",
            "date": "datetime64[ns]",
            "datetime": "datetime64[ns]",
        }
        return dtypes.get(self.type, "object")

    @property
    def parser(self) -> Callable:
        function = getattr(parsers, f"parse_{self.type}", None)
        if not function:
            raise NotImplementedError(f"Parsing not supported for type {self.type}")
        return function

    def parse(self, ds: pd.Series) -> Tuple[pd.Series, pd.Series]:
        function = self.parser
        argnames = function.__code__.co_varnames[1 : function.__code__.co_argcount]
        kwargs = {key: getattr(self, key) for key in argnames if hasattr(self, key)}
        parsed = function(ds, **kwargs)
        if parsed is ds:
            failed = slice(0, 0)
        else:
            failed = ds.notnull() & parsed.isnull()
        # NOTE: Assumes index is unique
        return parsed, ds[failed]
