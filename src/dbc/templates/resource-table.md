## `{{ resource.name }}`{% if resource.title %} {{ resource.title }}{% endif %}
{{ '\n' }}
{% if resource.description %}
  - `description` {{ resource.description | indent(4, False) }}
{% endif %}
{% if resource.path %}
  - `path` {{ resource.path }}
{% endif %}
{% if resource.schema %}
  - `schema`
{{ resource.schema | filter_dict(exclude=['fields']) | dict_to_markdown(level=2) }}
{% endif %}

{{ resource.schema.fields | tabulate() }}
